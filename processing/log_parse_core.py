import numpy as np
import matplotlib.pyplot as plt
import networkx as nx


def extract_lines(filename):
    with open(filename) as file:
        lines = file.readlines()
    for line in lines:
        line.strip()
    return lines


def extract_preamble(lines):
    clock_speed = float(lines[0].split(': ')[1])
    num_threads = int(lines[1].split(': ')[1])
    trotterisation = int(lines[2].split(': ')[1])
    graph_size = int(lines[3].split(': ')[1])
    num_qubits = int(lines[4].split(': ')[1])
    space_dimension = int(lines[5].split(': ')[1])
    run_type = int(lines[6].split(': ')[1])
    num_samples = int(lines[7].split(": ")[1])
    return clock_speed, num_threads, trotterisation, graph_size, num_qubits, space_dimension, run_type, num_samples, 8


def extract_preamble_new(lines):
    clock_speed = float(lines[0].split(': ')[1])
    num_threads = int(lines[1].split(': ')[1])
    trotterisation = int(lines[2].split(': ')[1])
    num_qubits = int(lines[3].split(': ')[1])
    space_dimension = int(lines[4].split(': ')[1])
    run_type = int(lines[5].split(': ')[1])
    num_samples = int(lines[6].split(": ")[1])
    method = int(lines[7].split(": ")[1])
    return clock_speed, num_threads, trotterisation, num_qubits, space_dimension, run_type, num_samples, method, 8


def extract_trial_type(lines, loc):
    num_deform = -1
    type = int(lines[loc].split(' ')[0])
    if type == 3 or type == 4 or type == 5:
        num_deform = int(lines[loc].split(' ')[2])
    return type, num_deform, loc + 1


def extract_graphs(lines, graph_size, loc):
    graph_1 = np.zeros((graph_size, graph_size))
    graph_2 = np.zeros((graph_size, graph_size))
    for i in range(loc + 2, loc + 2 + graph_size):
        line = lines[i].split(' ')
        for j in range(graph_size):
            if line[j] == '1':
                graph_1[i-7, j] = 1 #TODO: check if this is correct
    for i in range(loc + 2 + graph_size, loc + 2 + (2 * graph_size)):
        line = lines[i].split(' ')
        for j in range(graph_size):
            if line[j] == '1':
                graph_2[i-8-graph_size, j] = 1
    return graph_1, graph_2, 3+(2*graph_size)


def show_graph(graph):
    nx.draw(graph, node_size=500, with_labels=True)
    plt.show()


def build_graph(graph):
    rows, cols = np.where(graph == 1)
    edges = zip(rows.tolist(), cols.tolist())
    gr = nx.DiGraph()
    gr.add_edges_from(edges)
    return gr


# TODO: Check location
def extract_hamiltonian(lines, loc):
    hamiltonian = np.fromstring(lines[loc][1:-2], dtype=int, sep=' ')
    return hamiltonian, loc + 1


def extract_gbest(lines, loc):
    return -int(float(lines[loc].strip().split(' ')[1])), loc + 1


def extract_gbest_new(lines, loc):
    return int(float(lines[loc].strip().split(' ')[1])), loc + 1


def extract_aggregate(lines, loc):
    choices = np.fromstring(lines[loc + 1][1:-2], dtype=int, sep=' ')
    probabilities = np.fromstring(lines[loc + 2][1:-2], dtype=float, sep=' ')
    return choices, probabilities, loc + 3


def extract_solution(lines, loc):
    line = lines[loc].strip().split(' ')
    return -int(float(line[1])), int(line[2]), loc + 1


def extract_solution_new(lines, loc):
    line = lines[loc].strip().split(' ')
    return int(float(line[1])), int(line[2]), loc + 1


def extract_initial_expecations(lines, loc):
    classic = float(lines[loc].split(': ')[1])
    initial = float(lines[loc+1].split(': ')[1])
    return classic, initial, loc+2


def extract_termination(lines, loc):
    return int(lines[loc].strip()[0]), loc + 1


def parse_termination(term):
    if term == 1:
        return "SUCCESS"
    elif term == 2:
        return "MINF_MAX"
    elif term == 3:
        return "FTOL_REACHED"
    elif term == 4:
        return "XTOL_REACHED"
    elif term == 5:
        return "MAXEVAL_REACHED"
    elif term == 6:
        return "MAXTIME_REACHED"
    elif term == -1:
        return "FAILURE"
    elif term == -2:
        return "INVALID_ARGS"
    elif term == -3:
        return "OUT_OF_MEMORY"
    elif term == -4:
        return "ROUNDOFF_LIMITED"
    else:
        print("error")


def extract_expectation(lines, loc):
    return float(lines[loc].strip().split(' ')[1]), loc + 1


def extract_parameters(lines, loc):
    gammas = np.fromstring(lines[loc+2].strip()[2:-2], dtype=float, sep=' ')
    betas = np.fromstring(lines[loc+4].strip()[2:-2], dtype=float, sep=' ')
    return gammas, betas, loc + 5


# TODO: Check location
def extract_probabilities(lines, loc):
    line = lines[loc].strip()
    return np.fromstring(line[2:-2], dtype=float, sep=' '), loc + 1


def extract_num_evals(lines, loc):
    return int(lines[loc].strip().split(' ')[2]), loc + 1


def extract_timing(lines, loc):
    uc = float(lines[loc].strip().split(' ')[1])
    ub = float(lines[loc + 1].strip().split(' ')[1])
    op_time = float(lines[loc + 2].strip().split(' ')[1])
    tot_time = float(lines[loc + 3].strip().split(' ')[2])
    return uc, ub, op_time, tot_time, loc + 4
