import log_parse_optim_runs as pc
from matplotlib import pyplot as plt
import os
from matplotlib.ticker import MaxNLocator

ax = plt.figure().gca()

ax.xaxis.set_major_locator(MaxNLocator(integer=True))


plt.style.use('fivethirtyeight')

z = [0, 1, 2, 3, 4, 5]
width = 0.1
ax = plt.figure().gca()
ax.xaxis.set_major_locator(MaxNLocator(integer=True))
ax.set_xticks(range(3, 8))

# TODO: update passed arguments to include location not graph_size
def plot_error(results, location):
    for method in results:
        for accuracy in results[method]:
            xs = {}
            ys = {}
            # I want a per P set of results
            count = 0
            for trial in sorted(results[method][accuracy]):
                curr_entry = results[method][accuracy][trial]
                trott = curr_entry['trott']
                if trott not in xs:
                    xs[trott] = []
                    ys[trott] = []
                xs[trott].append(curr_entry['size']+(-3.5*width + trott*width))
                ys[trott].append(curr_entry['result_set']['error_av'])
                count += curr_entry['result_set']['num_trials']
            for series in xs:
                plt.bar(xs[series], ys[series], label='P = ' + str(series), width=width)
            plt.title("Error vs. Graph Size:\n" + method.split('_')[2])
            plt.figtext(.75, .025, "# Trials = "+str(count))
            plt.xlabel('Graph Size')
            plt.ylabel('Average Error')
            plt.legend(loc=6, bbox_to_anchor=(1, 0.5))
            #plt.savefig('/home/nicholas/Dropbox/Honours Project/Dissertation/resultplots/expect/error/'
            #            + method + accuracy + "error.svg", bbox_inches='tight')
            plt.tight_layout()
            plt.savefig(location+os.sep+method+"error.svg", bbox='tight', format='svg')
            plt.close()


def plot_ex_error(results, location):
    for method in results:
        for accuracy in results[method]:
            xs = {}
            ys = {}
            # I want a per P set of results
            count = 0
            for trial in sorted(results[method][accuracy]):
                curr_entry = results[method][accuracy][trial]
                trott = curr_entry['trott']
                if trott not in xs:
                    xs[trott] = []
                    ys[trott] = []
                xs[trott].append(curr_entry['size'])
                ys[trott].append(-curr_entry['result_set']['ex_error'])
                count += curr_entry['result_set']['num_trials']
            for series in xs:
                plt.plot(xs[series], ys[series], label='P = ' + str(series))
            plt.title("Expectation value error vs. Graph Size:\n" + method.split('_')[2])
            plt.figtext(.75, .025, "# Trials = "+str(count))
            plt.xlabel('Graph Size')
            plt.ylabel('Average Expectation Value Error')
            plt.legend(loc=6, bbox_to_anchor=(1, 0.5))
            # plt.savefig('/home/nicholas/Dropbox/Honours Project/Dissertation/resultplots/expect/time/'
            #            + method + accuracy + "time.svg", bbox_inches='tight')
            plt.tight_layout()
            plt.savefig(location+os.sep+method + "_ex_error.svg", bbox_inches='tight', format="svg")
            plt.close()


def plot_evals(results, location):
    for method in results:
        for accuracy in results[method]:
            xs = {}
            ys = {}
            # I want a per P set of results
            count = 0
            for trial in sorted(results[method][accuracy]):
                curr_entry = results[method][accuracy][trial]
                trott = curr_entry['trott']
                if trott not in xs:
                    xs[trott] = []
                    ys[trott] = []
                xs[trott].append(curr_entry['size'])
                ys[trott].append(curr_entry['result_set']['evals_av'])
                count += curr_entry['result_set']['num_trials']
            for series in xs:
                plt.plot(xs[series], ys[series], label='P = ' + str(series))
            plt.title("# Evaluations vs. Graph Size:\n" + method.split('_')[2])
            plt.figtext(.75, .025, "# Trials = "+str(count))
            plt.xlabel('Graph Size')
            plt.ylabel('Average #Evaluations')
            plt.legend(loc=6, bbox_to_anchor=(1, 0.5))
            # plt.savefig('/home/nicholas/Dropbox/Honours Project/Dissertation/resultplots/expect/evals/'
            #            + method + accuracy + "evals.svg", bbox_inches='tight')
            plt.tight_layout()
            plt.savefig(location+os.sep+method+"evals.svg", bbox_inches='tight', format='svg')
            plt.close()


def plot_time(results, location):
    for method in results:
        for accuracy in results[method]:
            xs = {}
            ys = {}
            # I want a per P set of results
            count = 0
            for trial in results[method][accuracy]:
                curr_entry = results[method][accuracy][trial]
                trott = curr_entry['trott']
                if trott not in xs:
                    xs[trott] = []
                    ys[trott] = []
                xs[trott].append(curr_entry['size'])
                ys[trott].append(curr_entry['result_set']['time_av'])
                count += curr_entry['result_set']['num_trials']
            for series in xs:
                plt.plot(xs[series], ys[series], label='P = ' + str(series))
            plt.title("Time vs. Graph Size:\n" + method.split('_')[2])
            plt.figtext(.75, .025, "# Trials = "+str(count))
            plt.xlabel('Graph Size')
            plt.ylabel('Average Time(s)')
            plt.legend(loc=6, bbox_to_anchor=(1, 0.5))
            # plt.savefig('/home/nicholas/Dropbox/Honours Project/Dissertation/resultplots/expect/time/'
            #            + method + accuracy + "time.svg", bbox_inches='tight')
            plt.tight_layout()
            plt.savefig(location+os.sep+method + "time.svg", bbox_inches='tight', format='svg')
            plt.close()


"""
ultrasults = pc.meta_parse("results")
pc.report(ultrasults)
plot_error(ultrasults)
print("Plotted Error")
# plot_evals(ultrasults)
# print("Plotted Evals")
# plot_time(ultrasults)
# print("Plotted Time")
"""

