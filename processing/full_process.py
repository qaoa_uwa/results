import log_parse_final_runs as pcf
import log_parse_optim_runs as pco
import plot_final as pf
import plot_master as pm
import os

location = "../desktop"
print("Beginning Initial Results")
ultrasults = pco.meta_parse(location+os.sep+"results")
num_initial = pco.report(ultrasults, location)
pm.plot_error(ultrasults, location)
print("Plotted Error")
pm.plot_evals(ultrasults, location)
print("Plotted Expectation error")
pm.plot_ex_error(ultrasults, location)

location = "../final"
print("Beginning Final Results")
ultrasults = pcf.meta_parse(location+os.sep+"results")
num_final = pcf.report(ultrasults, location)
pf.plot_error(ultrasults, location)
print("Plotted Error")
pf.plot_evals(ultrasults, location)
print("Plotted Expectation error")
pf.plot_ex_error(ultrasults, location)
print("Plotted Improvement")
pf.plot_improvement(ultrasults, location)
print("Plotted Comparison")
pf.plot_comparison(ultrasults, location)

location = "../final_undirect"
print("Beginning Final Undirected Results")
ultrasults = pcf.meta_parse(location+os.sep+"results")
num_undirect = pcf.report(ultrasults, location)
pf.plot_error(ultrasults, location)
print("Plotted Error")
pf.plot_evals(ultrasults, location)
print("Plotted Expectation error")
pf.plot_ex_error(ultrasults, location)
print("Plotted Improvement")
pf.plot_improvement(ultrasults, location)
print("Plotted Comparison")
pf.plot_comparison(ultrasults, location)

print("---------------Number of Trials---------------")
print("Initial: " + str(num_initial))
print("Final: " + str(num_final))
print("Undirected: " + str(num_undirect))
print("Total: " + str(num_initial + num_final + num_undirect))