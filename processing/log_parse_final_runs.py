import log_parse_core as pc
import os
import re
import math

accuracies = ['1e-18']
total_time = 0
total_qoptime = 0

def parse_trial(lines, graph_size, loc):
    trial_type, num_deform, loc = pc.extract_trial_type(lines, loc)
    loc += 2 + 2 * graph_size
    sol_val, sol_indx, loc = pc.extract_solution_new(lines, loc)
    classic, initial, loc = pc.extract_initial_expecations(lines, loc)
    gbest, loc = pc.extract_gbest_new(lines, loc)
    expectation, loc = pc.extract_expectation(lines, loc)
    gammas, betas, loc = pc.extract_parameters(lines, loc)
    choices, probabilities, loc = pc.extract_aggregate(lines, loc)
    termination, loc = pc.extract_termination(lines, loc)
    term_reason = pc.parse_termination(termination)
    num_evals, loc = pc.extract_num_evals(lines, loc)
    uc, ub, op_time, tot_time, loc = pc.extract_timing(lines, loc)
    return {
        "type": trial_type,
        "solution": sol_val,
        "choices": choices,
        "agg_probs": probabilities,
        "term_res": term_reason,
        "expect": expectation,
        "num_evals": num_evals,
        "op_time": op_time,
        "ex_error": expectation/sol_val,
        "gb_error": gbest/sol_val,
        'improvement': expectation - initial,
        'classic': classic,
        'comparison': (expectation - classic)/classic,
        'time': tot_time
    }, loc


def parse_file(filename):
    loc = 0
    num_trials = 0
    lines = pc.extract_lines(filename)
    graph_size = int(filename.split(os.sep)[-2].split('S')[1].split('P')[0])
    clock_speed, num_threads, trotterisation, num_qubits, space_dimension, run_type, num_samples, method, loc = pc.extract_preamble_new(lines)
    trial_data = []
    global total_time
    global total_qoptime
    if(graph_size < 3):
        print(filename)
    if run_type == 0:
        num_trials = 5
    elif run_type == 2:
        num_trials = 2
    else:
        num_trials = 1
    for i in range(num_trials):
        data, loc = parse_trial(lines, graph_size, loc)
        trial_data.append(data)
        total_time += (num_threads * data['time'])
        total_qoptime += data['num_evals']
    return trial_data


def parse_directory(path):
    results = []
    countreasons = {
        "SUCCESS": 0,
        "MINF_MAX": 0,
        "FTOL_REACHED": 0,
        "XTOL_REACHED": 0,
        "MAXEVAL_REACHED": 0,
        "MAXTIME_REACHED": 0,
        "FAILURE": 0,
        "INVALID_ARGS": 0,
        "OUT_OF_MEMORY": 0,
        "ROUNDOFF_LIMITED": 0
    }
    evals_av = 0
    evals_min = math.inf
    evals_max = -math.inf
    error_av = 0
    error_min = math.inf
    error_max = -math.inf
    time_av = 0
    time_min = math.inf
    time_max = -math.inf
    ex_error = 0
    improvement = 0
    comparison = 0
    for rfile in os.listdir(path):
        current = os.path.join(path, rfile)
        if os.path.isfile(current):
            curr_result = parse_file(current)
            for trial in curr_result:
                trial['filename'] = current
                if trial['num_evals'] < evals_min:
                    evals_min = trial['num_evals']
                if trial['num_evals'] > evals_max:
                    evals_max = trial['num_evals']
                evals_av += trial['num_evals']

                if trial['gb_error'] < error_min:
                    error_min = trial['gb_error']
                if trial['gb_error'] > error_max:
                    error_max = trial['gb_error']
                error_av += trial['gb_error']

                if trial['op_time'] < time_min:
                    time_min = trial['op_time']
                if trial['op_time'] > time_max:
                    time_max = trial['op_time']
                time_av += trial['op_time']

                countreasons[trial['term_res']] += 1

                ex_error += trial['ex_error']
                improvement += trial['improvement']
                comparison += trial['comparison']
                results.append(trial)
    evals_av /= len(results)
    error_av /= len(results)
    time_av /= len(results)
    ex_error /= len(results)
    improvement /= len(results)
    comparison /= len(results)
    return {
        'results': results,
        'evals_av': evals_av,
        'evals_min': evals_min,
        'evals_max': evals_max,
        'error_av': error_av,
        'error_min': error_min,
        'error_max': error_max,
        'time_av': time_av,
        'time_min': time_min,
        'time_max': time_max,
        'ex_error': ex_error,
        'improvement': improvement,
        'comparison': comparison,
        'count_reasons': countreasons,
        'num_trials': len(results)
    }


def meta_parse(directory):
    ultrasults = {}
    global total_time
    global total_qoptime
    total_time = 0
    total_qoptime = 0
    for method in os.scandir(directory):
        method_name = method.path.split(os.sep)[-1]
        ultrasults[method_name] = {}
        sub = os.scandir(method)
        for accuracy in sub:
            acc_indx = int(accuracy.path.split(os.sep)[-1]) - 1
            ultrasults[method_name][accuracies[acc_indx]] = {}
            sub2 = os.scandir(accuracy)
            for sp in sub2:
                sp_vals = re.findall(r"[1-9]+", sp.path.split(os.sep)[-1])
                ultrasults[method_name][accuracies[acc_indx]][sp.path.split(os.sep)[-1]] = {}
                ultrasults[method_name][accuracies[acc_indx]][sp.path.split(os.sep)[-1]]['size'] = int(sp_vals[0])
                ultrasults[method_name][accuracies[acc_indx]][sp.path.split(os.sep)[-1]]['trott'] = int(sp_vals[1])
                ultrasults[method_name][accuracies[acc_indx]][sp.path.split(os.sep)[-1]]['result_set'] = \
                    parse_directory(sp.path)
    print("------ Total Time ------")
    print(total_time)
    print("------ Optimiser Time ------")
    print(total_qoptime)
    return ultrasults


def report_old(ultrasults):
    with open("statreport.csv", "w+") as fout:
        fout.write("Method,Accuracy,Graph Size,Trotterisation,Average Evaluations,Minimum Evaluations,Maximum "
                   "Evaluations,Average Error,Minimum Error,Maximum Error,Average Time,Minimum Time,Maximum Time,"
                   "SUCCESS,MINF_MAX,FTOL_REACHED,XTOL_REACHED,MAXEVAL_REACHED,FAILURE,INVALID_ARGS,OUT_OF_MEMORY,"
                   "ROUNDOFF_LIMITED\n")
        for method in ultrasults:
            for accuracy in ultrasults[method]:
                for trial in ultrasults[method][accuracy]:
                    curr_entry = ultrasults[method][accuracy][trial]
                    fout.write(method+','
                               + accuracy+','
                               + str(curr_entry['size']) + ','
                               + str(curr_entry['trott']) + ','
                               + str(curr_entry['result_set']['evals_av']) + ','
                               + str(curr_entry['result_set']['evals_min']) + ','
                               + str(curr_entry['result_set']['evals_max']) + ','
                               + str(curr_entry['result_set']['error_av']) + ','
                               + str(curr_entry['result_set']['error_min']) + ','
                               + str(curr_entry['result_set']['error_max']) + ','
                               + str(curr_entry['result_set']['time_av']) + ','
                               + str(curr_entry['result_set']['time_min']) + ','
                               + str(curr_entry['result_set']['time_max']) + ','
                               + str(curr_entry['result_set']['count_reasons']['SUCCESS']) + ','
                               + str(curr_entry['result_set']['count_reasons']['MINF_MAX']) + ','
                               + str(curr_entry['result_set']['count_reasons']['FTOL_REACHED']) + ','
                               + str(curr_entry['result_set']['count_reasons']['XTOL_REACHED']) + ','
                               + str(curr_entry['result_set']['count_reasons']['MAXEVAL_REACHED']) + ','
                               + str(curr_entry['result_set']['count_reasons']['MAXTIME_REACHED']) + ','
                               + str(curr_entry['result_set']['count_reasons']['FAILURE']) + ','
                               + str(curr_entry['result_set']['count_reasons']['INVALID_ARGS']) + ','
                               + str(curr_entry['result_set']['count_reasons']['OUT_OF_MEMORY']) + ','
                               + str(curr_entry['result_set']['count_reasons']['ROUNDOFF_LIMITED']) + ','
                               + '\n')


def report(ultrasults, location):
    total_trials = 0
    for method in ultrasults:
        for accuracy in ultrasults[method]:
            for trial in ultrasults[method][accuracy]:
                curr_entry = ultrasults[method][accuracy][trial]
                total_trials += int(curr_entry['result_set']['num_trials'])
                filename = location+os.sep+"S"+str(curr_entry['size'])+"P"+str(curr_entry['trott'])+"_unweight"+".csv"
                try:
                    open(filename, 'r')
                except IOError:
                    with open(filename, 'w+') as fout:
                        fout.write("Method,Num Evals,Solution Error,Expectation Error,Comparison,Improvement,"
                                   "Num Trials\n")
                with open(filename, "a") as fout:
                    fout.write(method.split('_')[-1]+"," +
                               str(int(curr_entry['result_set']['evals_av'])) + "," +
                               str(round((float(curr_entry['result_set']['error_av'])), 4))+"," +
                               str(round((float(curr_entry['result_set']['ex_error'])), 4)) + "," +
                               str(round((float(curr_entry['result_set']['comparison'])), 4)) + "," +
                               str(round((float(curr_entry['result_set']['improvement'])), 4)) + "," +
                               str(curr_entry['result_set']['num_trials'])+"\n")
    return total_trials