import argparse
from log_parse_core import *


def main(arguments):
    print(arguments.filename)
    lines = extract_lines(arguments.filename)
# TODO: Changed passed arguments to location not graph_size
    clock_speed, num_threads, trotterisation, graph_size, num_qubits, space_dimension, run_type = extract_preamble(lines)
    graph_1, graph_2 = extract_graphs(lines, graph_size)
    nxG1 = build_graph(graph_1)
    nxG2 = build_graph(graph_2)
    extract_hamiltonian(lines, graph_size)
    sol_val, sol_indx = extract_solution(lines, graph_size)
    termination = extract_termination(lines, graph_size)
    term_reason = parse_termination(termination)
    expectation = extract_expectation(lines, graph_size)
    gammas, betas = extract_parameters(lines, 4)
    probabilities = extract_probabilities(lines, graph_size)
    num_evals = extract_num_evals(lines, graph_size)
    uc, ub, op_time, tot_time = extract_timing(lines, graph_size)

    print(clock_speed)
    print(num_threads)
    print(trotterisation)
    print(graph_size)
    print(num_qubits)
    print(space_dimension)
    print(graph_1)
    print(graph_2)
    print(nxG1, nxG2)
    print(sol_val, sol_indx)
    print(termination)
    print(term_reason)
    print(expectation)
    print(gammas)
    print(betas)
    print(probabilities)
    print(num_evals)
    print(uc, ub, op_time, tot_time)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Single QAOA outfile Parser")
    parser.add_argument('filename',
                        help='The name of the file you wish to process', metavar='f')
    args = parser.parse_args()
    main(args)
