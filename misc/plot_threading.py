import numpy as np
import math
import matplotlib.pyplot as plt
import matplotlib as mpl
import os
import scipy.optimize as opt
from matplotlib.ticker import MaxNLocator

ax = plt.figure().gca()

ax.xaxis.set_major_locator(MaxNLocator(integer=True))
plt.style.use('fivethirtyeight')

results = {}
results['ub'] = {}
results['uc'] = {}
results['expm'] = {}
results['dexpm'] = {}
for threads in ('1', '2', '4', '8', '12', '16', '24'):
    results['ub'][threads] = {}
    results['uc'][threads] = {}
    results['expm'][threads] = {}
    results['dexpm'][threads] = {}
    for g_size in ('5', '6', '7', '8', '9', '10'):
        results['ub'][threads][g_size] = [0, 0, 0]
        results['uc'][threads][g_size] = [0, 0, 0]
        results['expm'][threads][g_size] = [0, 0, 0]
        results['dexpm'][threads][g_size] = [0, 0, 0]



for file in os.scandir("../performance"):
    fileloc = os.path.abspath(file)
    filename = os.path.split(file)[1]
    task = filename.split('.csv')[0].split("__")[1]
    data = np.genfromtxt(fileloc, delimiter=',', names=['time', 'nnz', 'mem'])
    threads = filename.split('_')[1].split('S')[0]
    g_size = filename.split('_')[1].split('S')[1].split('P')[0]
    results[task][threads][g_size][0] += np.average(data['time'])
    results[task][threads][g_size][1] += int(np.average(data['nnz']))
    results[task][threads][g_size][2] += int(np.average(data['mem']))

print(results)
# We now have a full results array with everything organised nicely. We may now proceed to plot things


X = np.arange(5, 11)
Y = np.empty(6)
i = 0
qubits = []
side_len = []
nnz = []
j = 0
for i in range(5, 11):
    qubits.append(math.ceil(math.log2(math.factorial(i))))
    side_len.append(pow(2, qubits[j]))
    nnz.append(side_len[j] * qubits[j])
    j+=1


print(qubits)
print(side_len)
print(nnz)

fig, ax = plt.subplots()

def func(x, a, b, c):
    return a * np.exp(b * x) + c


for task in results:
    with open(task+"_threading.csv", "w+") as fout:
        fout.write("Thread Count, Graph Size, Time(s)\n")
        for threads in results[task]:
            Y = np.empty(6)
            i = 0
            for g_size in results[task][threads]:
                point = results[task][threads][g_size][0]
                Y[i] = point
                fout.write(threads + "," + g_size + "," + str(round(Y[i], 5))+"\n")
                i += 1
            plt.plot(X, Y, label=threads)
        plt.title(task.capitalize() + " vs. Graph size by Threads")
        optimisedParameters, pcov = opt.curve_fit(func, X, Y, bounds=[-1, 10])
        print(optimisedParameters)

        residuals = Y - func(X, *optimisedParameters)
        ss_res = np.sum(residuals ** 2)
        ss_tot = np.sum((Y - np.mean(Y))**2)
        r_squared = 1 - (ss_res / ss_tot)
        print(r_squared)

        plt.plot(X, func(X, *optimisedParameters), linestyle=':', label="$\\alpha e^{\\beta x} + c$\n$R^2 = $%f" % round(float(r_squared), 4))
        plt.legend()
        plt.ylabel("Time (s)")
        plt.xlabel("Graph Size")
        # plt.savefig('/home/nicholas/Dropbox/Honours Project/Dissertation/resultplots/threading/' +
                    # task+".png", bbox_inches='tight')
        plt.savefig(task+'.pdf', bbox_inches='tight')
        plt.savefig(task+'.svg', bbox_inches='tight')
        plt.clf()
        # plt.show()

for task in results:
    Ymin = np.empty(6)
    currMin = 1000
    minThreads = "0"
    for threads in results[task]:
        Y = np.empty(6)
        i = 0
        for g_size in results[task][threads]:
            point = results[task][threads][g_size][0]
            Y[i] = point
            i += 1
        if Y[5] < currMin:
            currMin = Y[5]
            Ymin = Y
            minThreads = threads
    plt.plot(X, Ymin, label=task.capitalize()+" "+minThreads)
plt.title("Timing by Task vs. Graph size")
plt.legend()
plt.ylabel("Time (s)")
plt.xlabel("Graph Size")
    # plt.savefig('/home/nicholas/Dropbox/Honours Project/Dissertation/resultplots/threading/' +
    # task+".png", bbox_inches='tight')
plt.savefig("timing"+'.pdf', bbox_inches='tight')
plt.savefig("timing"+'.svg', bbox_inches='tight')
plt.clf()


X = [1, 2, 4, 8, 12, 16, 24]
for task in results:
    with open(task+"_speedup.csv", "w+") as fout:
        fout.write("Thread Count, Speedup, Time\n")
        Y = np.empty(7)
        i = 0
        base = results[task]['1']['10'][0]
        for threads in results[task]:
            point = results[task][threads]['10'][0]
            Y[i] = base/point
            fout.write(threads + "," + str(round(Y[i], 5))+"\n")
            i += 1
        plt.plot(X, Y, label=task.capitalize())
plt.title("Speedup" + " vs. Number of Threads")
plt.legend(loc=2)
plt.ylabel("Speedup")
plt.xlabel("Threads")
plt.savefig("speedup" + '.pdf', bbox_inches='tight')
plt.savefig("speedup" + '.svg', bbox_inches='tight')
plt.clf()

for task in results:
    Y = np.empty(7)
    i = 0
    base = results[task]['1']['10'][0]
    for threads in results[task]:
        point = results[task][threads]['10'][0]
        Y[i] = base/point
        i += 1
    plt.plot(X, Y, label=task.capitalize())
    plt.title(task.capitalize()+" Speedup" + " vs. Number of Threads")
    plt.legend(loc=2)
    plt.ylabel("Speedup")
    plt.xlabel("Threads")
    plt.savefig(task + "_speedup" + '.pdf', bbox_inches='tight')
    plt.savefig(task + "_speedup" + '.svg', bbox_inches='tight')
    plt.clf()

# plt.show()



