import numpy as np
import math
import matplotlib.pyplot as plt
from sklearn.cluster import DBSCAN
from scipy.stats import kstest
from matplotlib.ticker import MaxNLocator

ax = plt.figure().gca()

ax.xaxis.set_major_locator(MaxNLocator(integer=True))
plt.style.use('fivethirtyeight')

num_points = 2000
x = range(1, num_points)
data = np.zeros((num_points, 2))
data2 = np.zeros(num_points)
for i in x:
    num_qubits = math.ceil(math.log2(math.factorial(i)))
    space_dimension = pow(2, num_qubits)
    dpoint = [i, (space_dimension - math.factorial(i))/math.factorial(i)]
    data[i-1] = dpoint
    data2[i-1] = dpoint[1]

clustering = DBSCAN(eps=0.3, min_samples=10).fit(data)

Dscore, pvalue = kstest(data2, lambda x: x)
print(Dscore, pvalue)

X = data
core_samples_mask = np.zeros_like(clustering.labels_, dtype=bool)
core_samples_mask[clustering.core_sample_indices_] = True
labels = clustering.labels_
unique_labels = set(labels)
n_clusters_ = len(set(labels)) - (1 if -1 in labels else 0)
colors = [plt.cm.Spectral(each)
          for each in np.linspace(0, 1, len(unique_labels))]
for k, col in zip(unique_labels, colors):
    if k == -1:
        # Black used for noise.
        col = [0, 0, 0, 1]

    class_member_mask = (labels == k)

    xy = data[class_member_mask & core_samples_mask]
    plt.plot(xy[:, 0], xy[:, 1], 'o', markerfacecolor=tuple(col),
             markeredgecolor='k', markersize=14)

    xy = X[class_member_mask & ~core_samples_mask]
    plt.plot(xy[:, 0], xy[:, 1], 'o', markerfacecolor=tuple(col),
             markeredgecolor='k', markersize=6)
plt.title('$\\frac{2^q - n!}{n!} - n!$ vs. n$')
plt.xlabel('n')
plt.ylabel('$2^q$')
plt.savefig("tail" + '.pdf', bbox_inches='tight')
plt.savefig("tail" + '.svg', bbox_inches='tight')
plt.show()
